variables:
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "-s mvn-settings.xml --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -P shade"
  # Used during gitflow interaction.
  MAJOR_RELEASE_DIGIT: 0
  MINOR_RELEASE_DIGIT: 1
  PATCH_RELEASE_DIGIT: 2

.git_template: &git_setup |
   git remote set-url --push origin "https://oauth2:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
   git config user.name "taxi-cd"
   git config user.email taxi-cd@vyne.co

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_REF_NAME"'
cache:
  paths:
    - .m2/repository

.setup-nvm: &setup-nvm
   - 'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash'
   - 'export NVM_DIR="$HOME/.nvm"'
   - 'unset NPM_CONFIG_PREFIX'
   - '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm'
   - 'nvm use stable'

.prepare-vscode-plugin-build: &prepare-vscode-plugin-build
   - 'cd language-server/vscode-extension'
   - 'npm install'
   - 'npm install -g vsce'
   - 'npm run sync-pom-version'

.cleanup-after-vscode-plugin-build: &cleanup-after-vscode-plugin-build
#   These files are modified during the build process, as we update their versions dynamically.
#   Currently, we don't commit the changes, although we could add that at a later date
   - 'cd language-server/vscode-extension'
   - 'git restore package.json package-lock.json'

stages:
   - build
   - build-plugin
   - trigger-release
   - deploy
   - deploy-plugin


# This will only validate and compile stuff and run e.g. maven-enforcer-plugin.
# Because some enforcer rules might check dependency convergence and class duplications
# we use `test-compile` here instead of `validate`, so the correct classpath is picked up.
validate:
  image: maven:3.3.9-jdk-8
  stage: build
  except:
     - tags
     - master
     - develop
  script:
    - 'mvn $MAVEN_CLI_OPTS clean deploy'
  artifacts:
    paths:
       - 'language-server/taxi-lang-server-standalone/target/taxi-lang-server-standalone.jar'
    reports:
      junit:
      # Note:This won't actually work, b/c of https://gitlab.com/gitlab-org/gitlab-runner/issues/2620
      # However, leaving this here to pick up later
        - "*/target/surefire-reports/TEST-*.xml"

## Manual jobs to trigger a release and increment the version (major,minor,patch)
release-major:
   variables:
      GIT_STRATEGY: none
   image: maven:3.3.9-jdk-8
   stage: trigger-release
   script:
      - *git_setup
      - 'mvn gitflow:release -B -DversionDigitToIncrement=$MAJOR_RELEASE_DIGIT -DskipTestProject=true'
   only:
      - develop
   when: manual

release-minor:
   variables:
      GIT_STRATEGY: none
   image: maven:3.3.9-jdk-8
   stage: trigger-release
   script:
      - *git_setup
      - 'mvn gitflow:release -B -DversionDigitToIncrement=$MINOR_RELEASE_DIGIT  -DskipTestProject=true'
   only:
      - develop
   when: manual

release-patch:
   variables:
      GIT_STRATEGY: none
   image: maven:3.3.9-jdk-8
   stage: trigger-release
   script:
      - *git_setup
      - 'mvn gitflow:release -B -DversionDigitToIncrement=$PATCH_RELEASE_DIGIT  -DskipTestProject=true'
   only:
      - develop
   when: manual

## Publishing tasks (snapshots and releases)
publish-snapshot-jars:
   stage: build
   script:
      - docker login -u vynecd -p $DOCKER_HUB_PASSWORD
      - 'mvn $MAVEN_CLI_OPTS deploy -P snapshot-release'
   only:
      - develop
   artifacts:
      paths:
         - "taxi-cli/target/*.zip"
         - 'language-server/taxi-lang-server-standalone/target/taxi-lang-server-standalone.jar'

build-plugin-beta:
   stage: build-plugin
   script:
      - *setup-nvm
      - *prepare-vscode-plugin-build
      - 'vsce package'
   only:
      - develop
   after_script:
      - *cleanup-after-vscode-plugin-build
   artifacts:
      paths:
         - 'language-server/vscode-extension/*.vsix'

publish-release:
   #image: maven:3.6.3-jdk-8
   stage: deploy
   script:
      - docker login -u vynecd -p $DOCKER_HUB_PASSWORD
      # Don't run tests on master, as already passed on develop
      # Use -DskipTests, but not -Dmaven.test.skip=true as we need to compile the tests (as other projects have test dependencies), but not run them.
      - 'mvn $MAVEN_CLI_OPTS -DskipTests deploy -P release'
      # Use jreleaser to publish to sdkman, and others
      - 'cd taxi-cli'
      - 'mvn jreleaser:full-release'
   only:
      - master
   artifacts:
      paths:
         - "taxi-cli/target/*.zip"
         - 'language-server/taxi-lang-server-standalone/target/taxi-lang-server-standalone.jar'

publish-plugin:
   stage: deploy-plugin
   script:
      - *setup-nvm
      - *prepare-vscode-plugin-build
      - 'vsce publish'
   only:
      - master
   artifacts:
      paths:
         - 'language-server/vscode-extension/*.vsix'
   after_script:
      - *cleanup-after-vscode-plugin-build
